/*
    This file is part of Filevary
    Copyright (C) 2013 Esteban Misael Maltauro

    Filevary is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Filevary is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 *
 * @author Esteban Misael Maltauro
 * Nota: partes de este codigo lo tenia guardado en mi lib personal, no recuerdo
 * quien fue que escribio este codigo pero yo lo modifique hasta que quedo a mi gusto.
 * es muy diferente al original ya que el original no funcionaba bien, pero igual
 * si alguien ve un codigo parecido a partes de este, mandeme un mensaje para añadir
 * dichos creditos a su correspondiente autor.
 */

package filevary;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import org.apache.commons.net.ftp.FTPFile;


public class Arbol extends JPanel{
    
 JTree treeRemoto;
 DefaultTreeModel modelo;
 DefaultMutableTreeNode Raiz;
 DefaultMutableTreeNode seleccionado;
 
 public boolean swroot=false;
 String ruta=""; //ruta del archivo seleccionado

 
    public Arbol(){
  
        setLayout(new BorderLayout());
  
        Raiz=new DefaultMutableTreeNode("Raiz");
        modelo=new DefaultTreeModel(Raiz);
      
        treeRemoto=new JTree(modelo);
        treeRemoto.addTreeSelectionListener(new TreeSelectionListener(){
 
   @Override
   public void valueChanged(TreeSelectionEvent arg0) {
     try{
        seleccionado=(DefaultMutableTreeNode)treeRemoto.getLastSelectedPathComponent();
     
        DefaultMutableTreeNode se=null;
     
       
         if(seleccionado!=null){
            String tempath = arg0.getPath().toString();
            String tempath2 = tempath.replace("[Raiz", "").replace("]", "").replace(", ", "/");
            
            ruta=tempath2; //archivo seleccionado del arbol: ej:/img/foto.jpg
      
            if(!seleccionado.isLeaf()){
                
                se=new DefaultMutableTreeNode(seleccionado.getUserObject());
     
                if(!se.toString().equalsIgnoreCase("Raiz")){
                    listarDirectorio(seleccionado,ruta);// listamos el directorio
                }
            }
          }
        }catch(NullPointerException npe){
         npe.printStackTrace();
        } 
    
    }});
      
    add(treeRemoto);
 }

  /*
   * Lista en un tree especifico el directorio que le pasemos.
   */
  public void listarDirectorio(DefaultMutableTreeNode padre,String directori){
      
   FTPFile[] ftpFiles;
   try {
    Main.init.ftp.changeWorkingDirectory(directori);
    ftpFiles = Main.init.ftp.listDirectories();
    
    for (FTPFile ftpFile : ftpFiles){
        if (ftpFile.getType() == FTPFile.DIRECTORY_TYPE) {
          DefaultMutableTreeNode d=new DefaultMutableTreeNode(ftpFile.getName());
          DefaultMutableTreeNode d2=new DefaultMutableTreeNode(".");
          d.add(d2);
          padre.add(d);
        }
     }
    
    ftpFiles = Main.init.ftp.listFiles();
    
    for (FTPFile ftpFile : ftpFiles){
        if(ftpFile.getType() == FTPFile.FILE_TYPE){
          DefaultMutableTreeNode d=new DefaultMutableTreeNode(ftpFile.getName());
          padre.add(d);
        }
    }
   }catch (IOException e) {
    e.printStackTrace();
   }
   treeRemoto.expandRow(0);
   Arbol.this.updateUI();
   Main.init.repaint();
      
  }
  
  /*
   * lista el directorio raiz con sus directorios y archivos
   * Nota: no lista los sub directorios
   */
  public void listaDirectorios(){
    
   FTPFile[] ftpFiles;
   try {
    ftpFiles = Main.init.ftp.listDirectories();
    
    for(FTPFile ftpFile : ftpFiles){
          if (ftpFile.getType() == FTPFile.DIRECTORY_TYPE) {
           DefaultMutableTreeNode d=new DefaultMutableTreeNode(ftpFile.getName());
           DefaultMutableTreeNode d2=new DefaultMutableTreeNode(".");
           d.add(d2);
           Raiz.add(d);
          }
    }
    ftpFiles = Main.init.ftp.listFiles();
    
    for(FTPFile ftpFile : ftpFiles){
        if (ftpFile.getType() == FTPFile.FILE_TYPE) {
          DefaultMutableTreeNode d=new DefaultMutableTreeNode(ftpFile.getName());
          Raiz.add(d);
        }
    }
   } catch (IOException e) {
    e.printStackTrace();
   }
   
   treeRemoto.expandRow(0);
   Arbol.this.updateUI();
   Main.init.repaint();
  }
 
  
}