/*
    This file is part of Filevary
    Copyright (C) 2013 Esteban Misael Maltauro

    Filevary is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Filevary is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 *
 * @author Esteban Misael Maltauro
 */

package filevary;

import java.awt.BorderLayout;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.apache.commons.net.ftp.FTP;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;


public class AddTab {
  
public JPanel codePanel;
public RSyntaxTextArea textArea [] = new RSyntaxTextArea [100];
public JLabel imagen[] = new JLabel [100];
int indexImagen = 1;
RTextScrollPane scrollPane;
public final String src="Temp/";
int indexText = 1;

/*
 * Abre un nuevo archivo en el tab: 
 * EJ: create("index.php","/foro/index.php","/foro");
 */
    public void create(String nameFile, String pathFile, String dirFile){
        
      codePanel = new JPanel(new BorderLayout()); //crea un nuevo panel
      String ext = parserExt(nameFile); //obtiene la ext del archivo
      
      switch(TypeExt.TypeFile(ext)){ //que tipo de archivo es?
          case TypeExt.Binary:
              JOptionPane.showMessageDialog(null, "Filevary no admite la edición de archivos binarios.", "Filevary", JOptionPane.ERROR_MESSAGE);
              return;
          case TypeExt.Image: //si es imagen
              downloadImagen(pathFile); //descarga la imagen    
              imagen[indexImagen] = new JLabel(new ImageIcon(src+nameFile));//se crea un index por si hay muchas imagenes  
              codePanel.add(imagen[indexImagen]);//añadimos la imagen al panel
              Main.init.tabbedPane.addTab(nameFile,null,codePanel,dirFile); //mandamos el panel al tab
              indexImagen++; //para nuevas imagenes
              indexText++;//nuevos codigos
              deleteFile(nameFile); //borramos la imagen de la carp Temp
              return;
          default: //!= a binario o imagen, definimos el textarea del codigo
              textArea[indexText] = new RSyntaxTextArea(20, 60); 
              break;
      }
      
      //Tipo de codigo segun su extension
      switch(TypeExt.NameExt(ext)){
          case TypeExt.PHP:
              textArea[indexText].setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PHP);
              break;
          case TypeExt.XML:
              textArea[indexText].setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
              break;
          case TypeExt.TML:
              textArea[indexText].setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_HTML);
              break;
          case TypeExt.HTM:
              textArea[indexText].setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_HTML);
              break;
          case TypeExt.CSS:
              textArea[indexText].setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CSS);
              break;
          default:
              textArea[indexText].setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PHP);
              break;
      }
  
      
      textArea[indexText].setCodeFoldingEnabled(true);
      textArea[indexText].setAntiAliasingEnabled(true);
      
      //pasamos al textarea el codigo descargado del FTP
      textArea[indexText].setText(downloadRead(pathFile));
      
      scrollPane = new RTextScrollPane(textArea[indexText]);
      
      scrollPane.setFoldIndicatorEnabled(true);
      codePanel.add(scrollPane);
      
      Main.init.tabbedPane.addTab(nameFile,null,codePanel,dirFile);
      
      textArea[indexText].setCaretPosition(0);
      indexText++;
    }
    
    /*descarga un archivo del ftp, extrae su codigo, borra el archivo y retorna el codigo
     *Su principal uso es en la funciona create->textArea.settext();
    **/
    public String downloadRead(String Urlfile){
      StringBuilder code = new StringBuilder();
      String textcode="";
      
      try {
          
           
           File downloadFile = new File(src+Main.init.arbolRemoto.seleccionado.getUserObject().toString());
           downloadFile.createNewFile();
           OutputStream streamOut = new BufferedOutputStream(new FileOutputStream(downloadFile));
           boolean success = Main.init.ftp.retrieveFile(Urlfile, streamOut);
           streamOut.close(); 
           
           FileReader lector=new FileReader(downloadFile);
           BufferedReader contenido=new BufferedReader(lector);
           
           while((textcode=contenido.readLine())!=null){
              code.append(textcode+"\n");
           }
           lector.close();
           
           downloadFile.delete();
           return code.toString();

           
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Error 0x1000 consulte al soporte.", "Filevary", JOptionPane.ERROR_MESSAGE);
            return "";
          } catch (IOException ex) {
              JOptionPane.showMessageDialog(null, "Error 0x1001 consulte al soporte.", "Filevary", JOptionPane.ERROR_MESSAGE);
                return "";
            } 
      
    }
    
    /*
     *Descarga una imagen a Temp, pero no la borra.
     *porque necesita ser enviada al tab.
     */
    public void downloadImagen(String Urlfile){
      
      try {
           Main.init.ftp.setFileType(FTP.BINARY_FILE_TYPE); //cambiamos a modo binario
           File downloadFile = new File(src+Main.init.arbolRemoto.seleccionado.getUserObject().toString());
           downloadFile.createNewFile();
           FileOutputStream fos = new FileOutputStream(src+Main.init.arbolRemoto.seleccionado.getUserObject().toString());
           boolean success = Main.init.ftp.retrieveFile(Urlfile, fos);
           fos.close(); 
          
      } catch (FileNotFoundException ex) {
           JOptionPane.showMessageDialog(null, "Error 0x1003 consulte al soporte.", "Filevary", JOptionPane.ERROR_MESSAGE);
        }catch (IOException ex) {
           JOptionPane.showMessageDialog(null, "Error 0x1002 consulte al soporte.", "Filevary", JOptionPane.ERROR_MESSAGE);
         } 
      
    }
    
    //usado para borrar archivos del Temp, generalmente las imagenes ya enviadas al Tab.
    public void deleteFile(String filePath){
        File downloadFile = new File(src+filePath);
        downloadFile.delete();
    }
    
    /*retorna la extencion de un archivo
     *Ejemplo: parserExt("index.php");
     *valor retornado: "php"
     */
    public String parserExt(String ext){
        int len = ext.length();
        String extension = ""+ext.charAt(len-3)+ext.charAt(len-2)+ext.charAt(len-1);
        return extension;
    }
    
    /*
     * Transfiere un archivo que se encuentra en un tab al servidor remoto.
     * Ej: transferSaveWrite("<html>hola</html>", "index.php", "/funciones");
     */
    public void transferSaveWrite(String codigo, String nameFile, String dirFile){
        
        try {
            File filePath = new File(src+nameFile);
            FileWriter upFile = new FileWriter(src+nameFile );
            upFile.write(codigo);
            upFile.close();
           
            FileInputStream fis = new FileInputStream(src+nameFile);
            Main.init.ftp.changeWorkingDirectory(dirFile); //cambiamos el directorio en el host
            
                if(Main.init.ftp.storeFile(nameFile,fis)){
                    //transferencia correcta..
                }else
                    JOptionPane.showMessageDialog(null, "Error al transferir archivo.", "Filevary", JOptionPane.ERROR_MESSAGE);
                
            fis.close();
            filePath.delete();
            
        } catch (FileNotFoundException e){
            e.printStackTrace();
          }catch (IOException e) {
            e.printStackTrace();
           }  
    }
    
    
}
