/*
    This file is part of Filevary
    Copyright (C) 2013 Esteban Misael Maltauro

    Filevary is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Filevary is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 *
 * @author Esteban Misael Maltauro
 */

package filevary;

public class TypeExt {

public static final int Binary = 1;
public static final int Image = 2;
public static final int Code = 3;
public static final int Null = 0;

public static String Img[] = {"jpg","png","gif"};
public static String Binario[] = {"exe","zip","rar"};
public static String Codigo[] = {"php","tml","htm"};

public static String Types[] = {"php","xml","tml","htm","css"};
public static final int PHP=1;
public static final int XML=2;
public static final int TML=3;
public static final int HTM=4;
public static final int CSS=5;

    /*
     * Retorna si es binary, image o code; segun su extension
     */
    public static int TypeFile(String ext){
     
        for(int i=0 ; i < Img.length ; i++){
        if(ext.equalsIgnoreCase(Img[i]))return Image;    
        }
        
        for(int i=0 ; i < Codigo.length ; i++){
        if(ext.equalsIgnoreCase(Codigo[i]))return Code; 
        }
        
        for(int i=0 ; i < Binario.length ; i++){
        if(ext.equalsIgnoreCase(Binario[i]))return Binary; 
        }
        
        return Null;
    }
    
    /*
     * retorna el tipo de archivo segun su extension, PHP,HTML,etc.
     * para hacer comparaciones usar:
     * if(TypeExt.PHP == TypeExt.NameExt("file.php")
     * retorna true
     */
    public static int NameExt(String ext){
        
        for(int i=0 ; i < Types.length ; i++){
        if(ext.equalsIgnoreCase(Types[i]))return i+1; 
        }
        return Null;
    }
    
}
