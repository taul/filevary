/*
    This file is part of Filevary
    Copyright (C) 2013 Esteban Misael Maltauro

    Filevary is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Filevary is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 *
 * @author Esteban Misael Maltauro
 */

package filevary;

import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;


public class Main {

public static Ventana init = new Ventana();

    public static void main(String[] args) {
     
    SwingUtilities.invokeLater(new Runnable() {
        public void run() {
              //cambiamos el Skin que trae por defecto
              init.setDefaultLookAndFeelDecorated(true);
              SubstanceLookAndFeel.setSkin("org.pushingpixels.substance.api.skin.ModerateSkin");
              entry();
        }
    });
            
    }
    
    
    public static void entry(){
        init.setExtendedState(JFrame.MAXIMIZED_BOTH);
        init.setVisible(true);
        init.setEnabled(false);
        eraseTemp(); //borra el contenido de la carpeta Temp
     
        new Conectorftp(); //pide la conexion
        
    }
    
    public static void initialize(){
        init.setEnabled(true); //conexion OK...habilitamos la ventana
        init.labelStatus.setText("Estado: Conectado"); //estamos conectados
        
    }
    
    private static void eraseTemp() {
        
        String direccion = "Temp";

        File directorio = new File(direccion);
        File f;
        if (directorio.isDirectory() && directorio.exists()) {
        
            String[] files = directorio.list();
            if (files.length > 0) {
                for (String archivo : files) {
      
                    f = new File(direccion + File.separator + archivo);
                    f.delete();
                    f.deleteOnExit();


                }
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Error, imposible acceder a Temp.", "Filevary", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }

    }
    

}
